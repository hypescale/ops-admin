/* eslint-disable import/first */
// const AmazonCognitoIdentity = require('amazon-cognito-identity-js')
import {
  AuthenticationDetails,
  CognitoUserPool,
  CognitoUser
} from 'amazon-cognito-identity-js'

// const AWS_REGION = 'eu-west-1'
const COGNITO_POOL_DATA = {
  UserPoolId: process.env.COGNITO_POOL_ID,
  ClientId: process.env.COGNITO_CLIENT_ID
}

export const state = () => ({
  userPool: [],
  authDetails: '',
  userData: '',
  cognitoUser: null,
  tokens: {
    accessToken: '',
    idToken: '',
    refreshToken: ''
  },
  username: '',
  errcode: '',
  attributes: [],
  authenticated: false
})

export const getters = {
  getStateAttributes: state => state.attributes
}

export const mutations = {
  signOut (state) {
    state.cognitoUser.signOut()
    state.sessionToken = null
    state.authenticated = false
    state.username = ''
    state.userPool = []
  },
  setAttributes (state, attributes) {
    state.attributes = attributes
    state.username = state.attributes.filter(function (Obj) {
      return Obj.Name === 'email'
    })[0].Value
  },
  setUsername (state, payload) {
    state.username = payload
  },
  signIn (state) {
    state.authenticated = true
  },
  setUserPool (state) {
    state.userPool = new CognitoUserPool(COGNITO_POOL_DATA)
  },
  setTokens (state, payload) {
    console.log(payload)
    state.tokens.accessToken = payload.getAccessToken().getJwtToken()
    state.tokens.idToken = payload.getIdToken().getJwtToken()
    state.tokens.refreshToken = payload.getRefreshToken().getToken()
  },
  setCognitoUser (state, payload) {
    state.cognitoUser = payload
  },
  setCognitoDetails (state, authData) {
    state.authDetails = new AuthenticationDetails(authData)
    state.userData = { Username: authData.Username, Pool: state.userPool }
    state.cognitoUser = new CognitoUser(state.userData)
  },
  setError (state, payload) {
    state.errcode = payload
  },
  clearError (state) {
    state.errcode = ''
  }
}

export const actions = {
  signIn ({ state, commit, dispatch }, authData) {
    if (process.env.DEVELOPMENT) {
      dispatch('signInLocal', authData)
    } else {
      commit('clearError')
      commit('setUserPool')
      commit('setCognitoDetails', authData)
      state.cognitoUser.authenticateUser(state.authDetails, {
        onSuccess: (result) => {
          console.log('sign in success')
          commit('setTokens', result)
          commit('signIn')
          this.$router.push('/profile')
          dispatch('getUserAttributes')
          dispatch('setLogoutTimer', 3600)
        },
        onFailure: (err) => {
          console.log('sign in failure')
          commit('setError', JSON.stringify(err.code))
        }
      })
    }
  },
  signInLocal ({ state, commit, dispatch }, authData) {
    const userAccessToken = window.btoa(authData.username + ':' + authData.password)

    this.$axios.setHeader('Authorization', `Basic ${userAccessToken}`)
    this.$axios.setHeader('Content-Type', 'application/json')
    this.$axios.$post(`${process.env.API_HOST}/authenticate`).then((result) => {
      window.localStorage.setItem('token', result.token)
      window.localStorage.setItem('role', result.role)
      commit('signIn')
      this.$router.push('/')
    }).catch((err) => {
      commit('setError', err)
      commit('ui/toggleSnackBar', 'asdf', { root: true })
    })
  },
  tryAutoSignIn ({ state, commit, dispatch }, isClient) {
    commit('setUserPool')
    const cognitoUser = state.userPool.getCurrentUser()
    if (cognitoUser != null) {
      commit('setCognitoUser', cognitoUser)
      state.cognitoUser.getSession(function (err, session) {
        if (err) {
          console.error(JSON.stringify(err))
        } else {
          commit('setTokens', session)
          commit('signIn')
          dispatch('getUserAttributes')
          dispatch('setLogoutTimer', 3600)
        }
      })
    } else {
      console.log(isClient)
    }
  },
  getUserAttributes ({ commit, dispatch }) {
    state.cognitoUser.getUserAttributes(function (err, attributes) {
      if (err) {
        console.error(JSON.stringify(err))
      } else {
        commit('setAttributes', attributes)
      }
    })
  },
  setLogoutTimer ({ state, commit, dispatch }, expirationTime) {
    setTimeout(() => {
      dispatch('signOut')
    }, expirationTime * 1000)
  },
  signOut ({ commit }) {
    commit('signOut')
    this.$router.push('/login')
  }
}
