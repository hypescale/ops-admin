export const state = () => ({
  carriers: []
})

export const getters = {
  carriers: state => state.carriers
}

export const mutations = {
  set: (state, carriers) => {
    state.carriers = carriers
  }
}

export const actions = {
  async create ({ commit, dispatch, rootGetters }, payload) {
    return await this.$axios.$post('http://localhost:4000/carrier', payload)
      .then(() => {
        dispatch('fetch')
      })
    // eslint-disable-next-line handle-callback-err
      .catch((err) => {
        // context.error(err)
      })
  },
  async fetch ({ commit, dispatch, rootGetters }) {
    return await this.$axios.$get('http://localhost:4000/carriers')
      .then((response) => {
        commit('set', response)
      })
      // eslint-disable-next-line handle-callback-err
      .catch((err) => {
        // context.error(err)
      })
  },
  async update ({ commit, dispatch, rootGetters }, payload) {
    return await this.$axios.$put(`http://localhost:4000/carrier/${payload.key}`, payload)
      .then(() => {
        dispatch('fetch')
        commit('ui/toggleSnackBar', null, { root: true })
      })
      // eslint-disable-next-line handle-callback-err
      .catch((err) => {
        // context.error(err)
      })
  },
  async delete ({ commit, dispatch, rootGetters }, payload) {
    return await this.$axios.$delete(`http://localhost:4000/carrier/${payload.key}`)
      .then(() => {
        dispatch('fetch')
      })
      // eslint-disable-next-line handle-callback-err
      .catch((err) => {
        // context.error(err)
      })
  }
}
