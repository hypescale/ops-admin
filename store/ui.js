export const state = () => ({
  snackbar: false
})

export const getters = {
  snackbar: state => state.snackbar
}

export const mutations = {
  toggleSnackBar: (state) => {
    state.snackbar = !state.snackbar
  }
}
