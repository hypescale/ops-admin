export const state = () => ({
  tasks: []
})

export const getters = {
  tasks: state => state.tasks
}

export const mutations = {
  set: (state, tasks) => {
    state.tasks = tasks
  }
}

export const actions = {
  async fetch ({ commit, dispatch, rootGetters }) {
    return await this.$axios.$get('http://localhost:4000/tasks')
      .then((response) => {
        commit('set', response)
      })
    // eslint-disable-next-line handle-callback-err
      .catch((err) => {
        // context.error(err)
      })
  }
}
