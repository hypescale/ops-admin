export default function ({ $axios, redirect }) {
  $axios.onRequest((config) => {
    const token = window.localStorage.token
    if (!token) {
      redirect('/login')
    } else {
      // eslint-disable-next-line dot-notation
      config.headers.common['Authorization'] = `Basic ${token}`
    }
  })

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}
